#!/bin/bash
# open git bash
#$ cd /c/vsrepo/logtest
#run: sh gitBash.sh
git branch
branchtag=$(git log -1 --pretty=%h)
echo Current source tag $branchtag
FILES=*
for file in $FILES
do echo $(basename $file)
done
#prompt user for commit message
read -r -p 'Commit message: ' desc
#track all files
#git add .
#track deletes
git add -u
git commit -m "$desc"
#push to origin
git push origin master
echo ++++++
branchtag=$(git log -1 --pretty=%h)
echo New source tag $branchtag
git tag "$branchtag"
git push origin "$branchtag"
echo +++++++++++++
#####Docker stuff now####
#NAME=dockerdelange/logtest
#IMG=$NAME:$branchtag
#LATEST=NAME:latest
#read -r -p 'Docker user: ' DockerUser  # prompt user for docker user
#read -r -p 'Docker password: ' DockerPassword  # prompt user for docker user
echo 1 Build image
echo -----------
#docker build -t $IMG .
echo 2 Tag image
echo -----------
#docker tag $IMG $LATEST
echo 3 Push image
echo -----------
#docker push $NAME
echo 4 login to dockerhub image
echo -----------
#docker log -u $DockerUser -p DockerPassword
read